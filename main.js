let balance;
let outstanding;
let pay;
let laptops;

const BASE_URL = "https://noroff-komputer-store-api.herokuapp.com/";

const balanceNode = document.getElementById("balance");
const loanNode = document.getElementById("loan");
const bankNode = document.getElementById("bank");
const workNode = document.getElementById("work");
const payNode = document.getElementById("pay");
const featuresNode = document.getElementById("features")

const laptopsNode = document.getElementById("laptops");
const laptopBox = document.getElementById("laptop");

const balanceSpan = document.getElementById("balanceSpan");
const paySpan = document.getElementById("paySpan");

loanNode.addEventListener("click", function () {
    if (balance == 0) {
        alert("YOU NEED TO HAVE MONEY, TO LOAN MONEY");
        return;
    }

    if (outstanding > 0) {
        alert("PAY BACK YOUR CURRENT LOAN, BEFORE TAKING A NEW ONE");
        return;
    }

    let amount = prompt("Please enter the amount you wish to loan", [
        balance * 2,
    ]);

    if (!/^\d+$/.test(amount)) { // CHECK FOR NUMBERS        
        alert("PLEASE ONLY ENTER NUMBERS");
        return;
    }
    if (amount > 2 * balance) { // CHECK FOR DOUBLE BALANCE        
        alert("THE NUMBER IS TOO HIGH");
        return;
    }
    outstanding = Number(amount);

    createNewOutstandingElement();
});
bankNode.addEventListener("click", function () {
    if (outstanding > 0) {
        let deducted = pay * 0.9;
        let difference = pay - deducted;
        setBalance(balance + deducted);

        let newOutstanding = outstanding - difference;

        if (newOutstanding < 0) {
            setBalance(balance + Math.abs(newOutstanding));
            setOutstanding(0);
        } else {
            setOutstanding(newOutstanding);
        }
    } else {
        setBalance(balance + pay);
    }
    setPay(0);
});
workNode.addEventListener("click", function () {
    setPay(pay + 100);
});

laptopsNode.addEventListener("change", function (e) {
    let selectedId = e.target.value;

    let selectedLaptop = laptops.find((laptop) => laptop.id == selectedId);
    setCurrentLaptop(selectedLaptop);
});

function createNewOutstandingElement() {
    const outstandingNode = document.getElementById("outstandingNode");
    if (outstandingNode != null)
        outstandingNode.parentNode.removeChild(outstandingNode);

    let newOutstandingDiv = document.createElement("div");
    newOutstandingDiv.id = "outstandingNode";

    // Create and Add outstanding span
    let text = document.createElement("span");
    text.textContent = "Outstanding";
    newOutstandingDiv.appendChild(text);

    // Create and Add outstanding amount span
    let amount = document.createElement("span");
    amount.textContent = outstanding + " Kr.";
    amount.style.float = "right";
    amount.id = "outstanding";
    newOutstandingDiv.appendChild(amount);

    balanceNode.after(newOutstandingDiv);
    createRepayLoanButton();
}
function createRepayLoanButton() {
    let repayNode = document.getElementById("repay");
    if (repayNode != null) repayNode.parentNode.removeChild(repayNode);

    let newButton = document.createElement("button");

    newButton.textContent = "Repay Loan";
    newButton.style.background = "rgb(129, 53, 53)";
    newButton.style.marginTop = "auto";
    newButton.id = "repay";
    newButton.addEventListener("click", repayLoan);

    payNode.after(newButton);
}

function repayLoan() {
    let newOutstanding = outstanding - pay;

    if (newOutstanding < 0) {
        setBalance(balance + Math.abs(newOutstanding));
        setOutstanding(0);
    } else {
        setOutstanding(newOutstanding);
    }
    setPay(0)
}

function setPay(newValue) {
    pay = newValue;
    paySpan.innerHTML = pay + " Kr.";
}
function setBalance(newValue) {
    balance = newValue;
    balanceSpan.innerHTML = balance + " Kr.";
}
function setOutstanding(newValue) {
    const outstandingSpan = document.getElementById("outstanding");
    outstanding = newValue;
    outstandingSpan.innerHTML = outstanding + " Kr.";

    // If new Outstanding ends up at 0
    if (newValue == 0) {

        // Remove repay node
        let repayNode = document.getElementById("repay");
        if (repayNode != null) repayNode.parentNode.removeChild(repayNode);

        // Remove outstanding node
        let outstandingNode = document.getElementById("outstandingNode");
        if (outstandingNode != null)
            outstandingNode.parentNode.removeChild(outstandingNode);
    }
}
function setLaptops(newArray) {
    laptops = newArray;

    // Create a new Option for the select, for each of the computers available
    laptops.forEach((laptop) => {
        let newOption = document.createElement("option");

        newOption.value = laptop.id;
        newOption.textContent = laptop.title;

        laptopsNode.appendChild(newOption);
    });
}
function setCurrentLaptop(current) {
    laptopBox.innerHTML = "";

    // Create and Add image
    let laptopImage = document.createElement("img");
    laptopImage.src = BASE_URL + current.image;
    laptopImage.alt = "Computer";
    laptopImage.onerror = () => { 
        laptopImage.src = BASE_URL + current.image.replace("jpg", "png")
    }
    laptopBox.append(laptopImage);

    // Create and Add description
    let description = document.createElement("div");
    let title = document.createElement("h2");
    title.textContent = current.title;
    description.append(title);
    let text = document.createElement("p");
    text.textContent = current.description;
    description.append(text);    
    laptopBox.append(description);

    // Create and Add Call To Action (Make Purchase)
    let callToAction = document.createElement("div");
    callToAction.className = "cta";

    let price = document.createElement("h3");
    price.textContent = current.price + " Kr.";
    callToAction.append(price);

    let btn = document.createElement("button");
    btn.textContent = "BUY NOW";
    btn.addEventListener("click", function() {
        if (balance < current.price) {
            alert("YOU DO NOT HAVE SUFFICIENT FUNDS, DO MORE WORK")
            return;
        }

        setBalance(balance - current.price)

        alert("CONGRATULATIONS ON PURCHASING A NEW LAPTOP")
    })
    callToAction.append(btn);
    laptopBox.append(callToAction);

    RecreateFeatures(current.specs)    
}

function RecreateFeatures(specs) {
    featuresNode.innerHTML = ""

    let featuresTitle = document.createElement("h4")
    featuresTitle.textContent = "Features:"
    featuresNode.appendChild(featuresTitle)

    specs.forEach(spec => {
        let specElement = document.createElement("p")
        specElement.textContent = spec;
        featuresNode.appendChild(specElement)
    })
}

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then((res) => res.json())
    .then((json) => {
        setLaptops(json);
        let initialLaptop = laptops[0];
        setCurrentLaptop(initialLaptop);
    });

if (outstanding > 0) {
    createNewOutstandingElement();
}

setPay(0);
setBalance(0);
